var admin = require("firebase-admin");
var serviceAccount = require("./config/cups-firebase-secret.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://cups-e78c9.firebaseio.com"
});
  
const checkAuth = (req, res, next) => {
    if (process.env.NODE_ENV === 'development') {
        next()
    } else {
        if (req.headers.authtoken) {
            admin.auth().verifyIdToken(req.headers.authtoken)
            .then(() => {
                next()
            })
            .catch(() => {
                res.status(403).send('Unauthorized!')
            })
        } else {
            res.status(403).send('Unauthorized!')
        }
    }
}

module.exports = checkAuth