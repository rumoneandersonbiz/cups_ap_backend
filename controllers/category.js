const knex = require('../data/db');

const createCategory = () => async (req, res) => {
    try {
        const manager = await knex.select().from('manager').first()
        
        // get manager id
        const newCategory = await knex('category').insert({
            manager_id: manager.manager_id,
            category_name: req.body.categoryName,
        }).returning("category_name")

        res.send({message: "New Category successfully added", data: newCategory});
    } catch (error) {
        res.send({message: "An error occurred when adding category"});
    }
}

//return all categories
const allCategories = () => async (req, res) => {
    try {
        const items = await knex.select().from('category');
        res.send(items)
    } catch (error) {
        res.send("something went wrong")
        console.log(error)
    }
}

//update category
const updateCategory = () => async(req, res) => {
    const { category_id } = req.body;
    
    const up = await knex('category')
    .where({category_id})
    .update({
        category_name: req.body.categoryName,
    },)
    
    res.json({success:true, up:up, messages: "Updated Successful"})
}

//delete category
const deleteCategory = () => async (req, res) => {
    const { category_id } = req.body;

    const del = await knex('category')
    .del()
    .where({ category_id })

    res.json({success:true, delete:del, messages: "Deleted Successful"})
}
//get single category
const getCategory = () => async(req,res) =>{

    const{ category_id } = req.body

    const getCat = await knex('item')
    .where({category_id})
    res.send(getCat)
}

module.exports = {
    createCategory,
    allCategories,
    updateCategory,
    deleteCategory,
    getCategory
}