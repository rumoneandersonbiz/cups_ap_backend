const knex = require("../data/db");
const _ = require("lodash/core");
// handle manager login
const customer = () => async (req, res) => {
    try {
        const user_id = await knex('application_user').insert({
            firstName: req.body.firstName,
            lastName: req.body.lastName
        })
        .returning('user_id');
        // updates the manager table    
        const customer_id = await knex('customer').insert({
            user_id: parseInt(user_id, 10),
            cups_coin: 500,
            isAuth: false,
            disability: req.body.disability,    
        }).returning('customer_id');
        
        return res.status(201).json({customerId: parseInt(customer_id, 10)});
    } catch (error) {
        return res.status(400).send("Customer not created");
    }
};

const uploadGesture = () => async (req, res) => {
    try {
        const id = req.params.id;
        let gesture = req.files.gesture;
        if (!gesture) {
            return res.status(400).send('No files were sent');
        }

        // updates the customer table
        const customer_id = await knex('customer')
        .where({customer_id: id})
        .update({
            isAuth: true,
            auth_gesture_hash: gesture.md5
        }).returning("customer_id");

        return res.status(200).send({customerId: parseInt(customer_id, 10)})
    } catch (error) {
        return res.status(400).send("Error occurred")
    }
};


const getCustomers = () => async (req, res) => {
    try {
        const customers = await knex('customer').orderBy('customer_id');
        return res.status(200).json(customers);
    } catch (error) {
        return res.sendStatus(400).send("Error retrieving customers")
    }
};


const getCustomer = () => async (req, res) => {
    try {
        const authPhoto = req.files.authPhoto;
        if (!authPhoto) {
            return res.status(400).status('No files were uploaded')
        }
        
        const customer = await knex.select(
            'customer.customer_id',
            'application_user.firstName',
            'application_user.lastName',
            'customer.disability',
            'customer.cups_coin'
        )
        .from('customer')
        .innerJoin('application_user', 'customer.user_id', 'application_user.user_id')
        .where({auth_gesture_hash: authPhoto.md5})
        .first()

        if(_.isEmpty(customer)){
            return res.status(400).send('User is not in the system')
        }

        return res.status(200).json(customer)
    } catch (error) {
        console.log(error)
        return res.status(400)
    }
}

module.exports = {
    customer,
    getCustomers,
    uploadGesture,
    getCustomer
};