const knex = require("../data/db");
const _ = require("lodash/core")

const getOrders = () => async (req, res) => {
    try {

        const today = new Date();
        const orders = await knex.select(
            "order.order_id",
            "order.order_time",
            "order.is_complete",
            "order.is_cancelled",
            "item.item_name",
            "item.item_photo",
            "customer.disability",
            "application_user.firstName",
            "application_user.lastName"
        ).from("customer_order as order")
        .leftJoin("customer", "order.customer_id", "customer.customer_id")
        .leftJoin("item", "order.item_id", "item.item_id")
        .leftJoin("application_user", "customer.user_id", "application_user.user_id")
        .where({is_complete: false})
        .where({is_cancelled: false})
        // .where({order_time: today})

        return res.status(200).send(orders);
    } catch (e) {
        return res.status(500).send('There was an error retrieving orders')
    }
};


const newOrderDeaf = () => async (req, res) => {
    // accepts an array orders
    try {
        // get auth photo from request body
        const authPhoto = req.files.authPhoto
        // get item photo from reques body
        const itemPhoto = req.files.itemPhoto

        const customer = await knex('customer')
        .where({
            auth_gesture_hash: authPhoto.md5
        })
        .first()
        
        if (_.isEmpty(customer)) {
            return res.status(400).send('Me nuh know you')
        }


        const item = await knex('item').where({
            sign_photo_hash: itemPhoto.md5  
        })
        .andWhere({is_deleted: false})
        .first();        

        if (_.isEmpty(item)) {
            return res.status(400).send('Dat nuh deh yah')
        }

        const cost = parseFloat(item.item_cost);
        const funds = parseFloat(customer.cups_coin);
        let itemAmt = parseInt(item.quantity, 10)
        let balance = 0;


        if (itemAmt > 0) {
            itemAmt--;
        } else {
            return  res.status(400).send('Item is out of stock')
        }

        if (cost > funds) {
            return res.status(400).send('You need more cups coins')
        } else {
            balance = funds - cost;
        }

        await knex('item')
        .where({item_id: item.item_id})
        .update({
            quantity: itemAmt
        })

        await knex('customer')
        .where({customer_id: customer.customer_id})
        .update({
            cups_coin: balance
        })

        const newOrder = await knex('customer_order').insert({
            customer_id: customer.customer_id,
            item_id: item.item_id,
            order_time: new Date(),
            is_complete: false,
            is_cancelled: false
        }).returning('order_id');
        
        return res.status(201).send({orderId: newOrder})

    } catch (e) {
        console.log(e)
        return res.status(500).send('Error occurred placing order')
    }
};


const newOrderBlind = () => async(req, res) => {
    try {
        const authPhoto = req.files.authPhoto
        const itemAudio = req.files.itemAudio

        const customer = await knex('customer')
        .where({
            auth_gesture_hash: authPhoto.md5
        })
        .first()
        
        if (_.isEmpty(customer)) {
            return res.status(400).send('Me nuh know you')
        }
        
        const item = await knex('item').where({
            item_audio_hash: itemAudio.md5  
        })
        .andWhere({is_deleted: false})
        .first();          

        if (_.isEmpty(item)) {
            return res.status(400).send('Dat nuh deh yah')
        }

        const cost = parseFloat(item.item_cost);
        const funds = parseFloat(customer.cups_coin);
        let itemAmt = parseInt(item.quantity, 10);
        let balance = 0;

        if (itemAmt > 0) {
            itemAmt--;
        } else {
            return res.status(400).send('Item is out of stock')
        }

        if (cost > funds) {
            return res.status(400).send('You need more cups coins')
        } else {
            balance = funds - cost;
        }

        await knex('customer')
        .where({customer_id: customer.customer_id})
        .update({
            cups_coin: balance
        })

        await knex('item')
        .where({item_id: item.item_id})
        .update({
            quantity: itemAmt
        })

        const newOrder = await knex('customer_order').insert({
            customer_id: customer.customer_id,
            item_id: item.item_id,
            order_time: new Date(),
            is_complete: false,
            is_cancelled: false
        }).returning('order_id');
        


        return res.status(201).send(newOrder)
    } catch (e) {
        return res.status(500).send('Error occurred placing order')
    }
}


const getCustomerOrder = () => async(req, res) => {
    try {
        const {id} = req.params;

        const orders = await knex.select(
            'order.order_id',
            'item.item_name',
            'item.item_cost'
        )
        .from('customer_order as order')
        .innerJoin('item', 'order.item_id', 'item.item_id')
        .where({customer_id: id})
        .where({is_complete: false})
        .where({is_cancelled: false})

        res.status(200).send(orders)
    } catch (error) {
        console.log(error)
        res.status(400).send('Error retreiving customer orders')
    }
}


const cancelOrder = () => async(req, res) => {
    try {
        const {id} = req.params;

        await knex('customer_order')
        .where({order_id: id})
        .update({
            is_cancelled: true
        })

        return res.status(200).send('Order was cancelled')

    } catch (e) {
        return res.status(400).send('Error occurred placing order')
    }
} 

const completeOrder = () => async(req, res) => {
    try {
        const {id} = req.params;

        await knex('customer_order')
        .where({order_id: id})
        .update({
            is_complete: true
        })

        return res.status(200).send('Order is complete')
    } catch (e) {
        return res.status(400).send('Error occurred placing order')
    }
}


module.exports = {
    getOrders,
    newOrderDeaf,
    newOrderBlind,
    cancelOrder,
    completeOrder,
    getCustomerOrder,
};