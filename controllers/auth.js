const knex = require("../data/db");

const manageAuth = () => async (req, res) => {
    try {
        const user_id = await knex('application_user').insert({
            firstName: req.body.firstName,
            lastName: req.body.lastName
        })
        .returning('user_id');

        const newManager = await knex('manager').insert({
            user_id: parseInt(user_id, 10),
            manager_email: req.body.email,
            is_current:true
        }).returning('manager_id');

        return res.status(200).send(newManager)
    } catch (error) {
        console.log(error)
        return res.status(400).send('Error occured added you to the system, contact developer')
    }
}


const getManager = () => async(req, res) => {
    try {
        const currentManager = await knex.select(
            "application_user.firstName",
            "application_user.lastName",
        ).from('manager')
        .innerJoin("application_user", "application_user.user_id", "manager.user_id")
        .where({is_current: true})
        .first()

        res.status(200).send(currentManager);
    } catch (e) {
        console.log(e)
        res.status(500).send("There was an error retreiving manager information")
    }
}


module.exports = {
    manageAuth,
    getManager
}