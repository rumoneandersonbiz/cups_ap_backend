const knex = require("../data/db");


const reportSales = () => async (req, res) => {
    try {
        const orders = await knex.select(
            "item.item_name",
            "item.item_cost"
        ).from("customer_order as order")
        .leftJoin("item", "order.item_id", "item.item_id")
        .where({is_complete: true})

        const reportData = countSalesReport(orders);

        return res.status(200).send(reportData)
    } catch (e) {
        return res.status(500).send('Error occurred generating report')
    }
}

const reportCategories = () => async (req, res) => {
    try {
        const orders = await knex.select(
            "item.item_name",
            "item.item_cost",
            "category.category_name"
        ).from("customer_order as order")
        .leftJoin("item", "order.item_id", "item.item_id")
        .leftJoin("category", "order.item_id", "category.category_id")
        .where({is_complete: true})

        // reduce amount for each category
        let beverageCount = 0;
        let foodCount = 0;

        orders.forEach(item => {
            console.log(item)
            if (item.category_id === '1'){
                beverageCount++;
            } else {
                foodCount++
            }
        })

        return res.status(200).send({
            beverage: beverageCount,
            food: foodCount
        })          
    } catch (e) {
        
        return res.status(500).send('Error occurred generating report')
    }
}

const reportCustomers = () => async (req, res) => {
    try {
        const customers = await knex.select(
            "application_user.firstName",
            "application_user.lastName",
            "item.item_cost",
            "customer.customer_id"
        ).from("customer_order as order")
        .leftJoin("customer", "order.customer_id", "customer.customer_id")
        .leftJoin("item", "order.item_id", "item.item_id")
        .leftJoin("application_user", "customer.customer_id", "application_user.user_id")
        .where({is_complete: true})

        return res.status(200).send(customers)
    } catch (e) {
        return res.status(500).send('Error occurred generating report')
    }
}

const countSalesReport = (Arr) => {
    let reportArr = [];
    let salesAmt = 0;
    let copy = Arr;

    const numSales = Arr.length;

    Arr.forEach(element => {
        let itemCount = 0;
        let addItem = true;

        copy.forEach(elem => {
            if (element.item_name.toLowerCase() === elem.item_name.toLowerCase()) {
                itemCount++;
            }
        });

        reportArr.forEach(elem => {
            if (elem.item_name.toLowerCase() === element.item_name.toLowerCase()) {
                addItem = false;
            }    
        });

        salesAmt+=parseFloat(element.item_cost)

        const item = {
            item_name: element.item_name,
            item_count: itemCount,
            salesPercentage: itemCount/numSales
        }
        if (addItem) {
            reportArr=[...reportArr, item];
        }

        addItem = true;
    });

    return {
        salesAmt,
        reportArr,
        numSales
    }
}



module.exports = {
    reportSales,
    reportCustomers,
    reportCategories
}