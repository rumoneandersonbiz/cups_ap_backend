const knex = require('../data/db');
const path = require('path');
const _ = require("lodash/core")

const createItem = () => async (req, res) => {
    // gets object for new item
    try {
        const manager = await knex('manager')
        .where({is_current: true})
        .first()

        if(_.isEmpty(manager)) {
            return res.status(400).send('There is no managers currently')
        }

        const signPhoto = req.files.sign_photo;
        const itemPhoto = req.files.item_photo;
        const itemAudio = req.files.item_audio;

        // gets the current date
        const today = new Date();
        const dateFormatted = today.toString().split(" ").slice(1, 5).join("-").toUpperCase();

        const signPhotoName = dateFormatted + "-" + signPhoto.name.toUpperCase();
        const itemPhotoName = dateFormatted + "-" + itemPhoto.name.toUpperCase();
        const itemAudioName = dateFormatted + "-" + itemAudio.name.toUpperCase();

        const rootDir = path.dirname(require.main.filename);

        // item directory
        const signPhotoDirectory = `/items/${signPhotoName}`;
        const itemPhotoDirectory = `/items/${itemPhotoName}`;
        const itemAudioDirectory = `/items/${itemAudioName}`;

        signPhoto.mv(`${rootDir}/uploads/${signPhotoDirectory}`);
        itemPhoto.mv(`${rootDir}/uploads/${itemPhotoDirectory}`);
        itemAudio.mv(`${rootDir}/uploads/${itemAudioDirectory}`);

        // updates the database
        const newItem = await knex('item').insert({
            manager_id: manager.manager_id,
            category_id: req.body.category_id,
            item_name: req.body.item_name,
            item_cost: req.body.item_cost,
            quantity: req.body.quantity,
            sign_photo: signPhotoDirectory,
            sign_photo_hash: signPhoto.md5,
            item_photo: itemPhotoDirectory,
            item_photo_hash: itemPhoto.md5,
            item_audio: itemAudioDirectory,
            item_audio_hash: itemAudio.md5
        }).returning('*')


        res.status(201).send(newItem[0])
    } catch (error) {
        res.status(400).send("Error adding new items")
    }
};


const editItem = () => async(req, res) => {
    // gets object for new item
    try {
        const item_id = req.params.id;

        // console.log(manager, item_id)

        const item = await knex('item')
        .where({item_id})
        .update({
            category_id: req.body.category_id,
            item_name: req.body.item_name,
            item_cost: req.body.item_cost,
            quantity: req.body.quantity
        }).returning("item_id")

        return res.status(204).send('Item update was successfule')
    } catch (error) {
        return res.status(400).send("Error adding new items")
    }
}

const deleteItem = () => async(req, res) => {
    // gets object for new item
    try {
        const item_id = req.params.id;

        const item = await knex('item')
        .where({item_id})
        .update({
            is_deleted: true,
        })
        .returning("item_id")

        return res.status(200).send(item)
    } catch (error) {
        return res.status(400).send("Error adding new items")
    }
}


//get item
const getItem = () => async(req,res) =>{
    const { id } = req.params;
    try {
        const item = await knex('item').where({item_id:id}).first();
        const category = await knex('category').where({category_id: item.category_id}).first();

        const data = {
            ...item,
            category,
        };

        res.status(200).send(data);
    } catch (e) {
        console.log(e);
        res.status(400).send("Error retrieving item information");
    }
};

// update items
const updateItem = () => async(req, res) => {
    const {item_id} = req.body;
    
    const up = await knex('item')
    .where({item_id})
    .update({
        category_id: req.body.categoryId,
        item_name: req.body.itemName,
        item_cost: req.body.itemCost,
        quantity: req.body.quantity,
        sign_photo: req.body.signPhoto,
        item_photo: req.body.itemPhoto,
        item_audio: req.body.itemAudio
    });
    
    res.json({success:true, up:up, messages: "Updated Successful"})
};

// return all items
const allItems = () => async (req, res) => {
    // get all items
    try {
        const items = await knex.select().from("item").where({is_deleted: false})
        return res.status(200).json(items)
    } catch (error) {
        return res.status(400).send("something went wrong");
    }
};



module.exports = {
    createItem,
    allItems,
    deleteItem,
    editItem,
    updateItem,
    getItem
};