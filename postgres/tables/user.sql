BEGIN TRANSACTION;

CREATE TABLE application_user(
    user_id serial PRIMARY KEY,
    user_firstname VARCHAR(100),
    user_lastname VARCHAR(100)
);

COMMIT;