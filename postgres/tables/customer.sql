BEGIN TRANSACTION;

CREATE TABLE customer (
    customer_id SERIAL UNIQUE NOT NULL,
    user_id int NOT NULL UNIQUE,
    cups_coin DECIMAL NOT NULL,
    disability VARCHAR(5) CHECK ( disability='BLIND' OR disability='DEAF' ) NOT NULL,
    isAuth BOOLEAN NOT NULL,
    auth_gesture VARCHAR(255),
    auth_gesture_hash VARCHAR(255),

    PRIMARY KEY (customer_id, user_id),
    FOREIGN KEY (user_id) REFERENCES application_user(user_id)
);
COMMIT;