BEGIN TRANSACTION;

CREATE TABLE customer_order (
    order_id serial NOT NULL UNIQUE,
    customer_id int NOT NULL,
    item_id int NOT NULL,
    order_time TIMESTAMP NOT NULL,
    is_complete BOOLEAN DEFAULT false,
    is_cancelled BOOLEAN DEFAULT false,

    PRIMARY KEY(order_id, customer_id, item_id),
    FOREIGN KEY (customer_id) REFERENCES customer(customer_id),
    FOREIGN KEY (item_id) REFERENCES item (item_id)
);

COMMIT;
