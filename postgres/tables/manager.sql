BEGIN TRANSACTION;

CREATE TABLE manager (
    user_id int NOT NULL,
    manager_id SERIAL NOT NULL UNIQUE,
    manager_email VARCHAR(100) NOT NULL,
    
    PRIMARY KEY (manager_id, user_id),
    FOREIGN KEY (user_id) REFERENCES application_user (user_id)
);

COMMIT;