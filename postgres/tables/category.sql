BEGIN TRANSACTION;

CREATE TABLE category (
    category_id serial UNIQUE NOT NULL,
    category_name VARCHAR(255) NOT NULL,
    PRIMARY KEY (category_id)
);

COMMIT;