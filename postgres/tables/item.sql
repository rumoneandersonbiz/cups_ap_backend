BEGIN TRANSACTION;

CREATE TABLE item (
    item_id SERIAL NOT NULL UNIQUE,
    manager_id int NOT NULL,
    category_id int NOT NULL,
    item_name VARCHAR(255) NOT NULL,
    item_cost DECIMAL NOT NULL,
    quantity int NOT NULL,
    sign_photo VARCHAR(255) NOT NULL,
    item_photo VARCHAR(255) NOT NULL,
    item_audio VARCHAR(255) NOT NULL,
    sign_photo_hash VARCHAR(130) NOT NULL,
    item_photo_hash VARCHAR(130) NOT NULL,
    item_audio_hash VARCHAR(130) NOT NULL,


    PRIMARY KEY (item_id),
    FOREIGN KEY (manager_id) REFERENCES manager (manager_id),
    FOREIGN KEY (category_id) REFERENCES category (category_id)
);


COMMIT;
