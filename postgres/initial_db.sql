-- run user table query
\i '/docker-entrypoint-initdb.d/tables/user.sql'
\i '/docker-entrypoint-initdb.d/tables/manager.sql'
\i '/docker-entrypoint-initdb.d/tables/category.sql'
\i '/docker-entrypoint-initdb.d/tables/item.sql'
\i '/docker-entrypoint-initdb.d/tables/customer.sql'
\i '/docker-entrypoint-initdb.d/tables/order.sql'

