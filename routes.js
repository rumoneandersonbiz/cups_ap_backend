const routes = require('express').Router();
const checkAuth  = require('./firebase');

// controllers
const itemsController = require('./controllers/Items');
const managerAuthController = require('./controllers/auth');
const categoryController = require('./controllers/category');
const customerController = require('./controllers/customer');
const orderController = require('./controllers/order');
const reportController = require('./controllers/report');

// order routes
routes.post('/order/deaf', orderController.newOrderDeaf());
routes.post('/order/blind', orderController.newOrderBlind());
routes.get('/order', orderController.getOrders())
routes.get('/order/:id', orderController.getCustomerOrder());
routes.get('/order/cancel/:id', orderController.cancelOrder());
routes.get('/order/complete/:id', orderController.completeOrder());


// item routes
routes.get('/item', checkAuth, itemsController.allItems());
routes.get('/item/:id', checkAuth, itemsController.getItem());
routes.post('/item', checkAuth, itemsController.createItem());
routes.put('/item', checkAuth, itemsController.updateItem());

routes.post('/item/:id', checkAuth, itemsController.editItem());
routes.delete('/item/:id', checkAuth, itemsController.deleteItem());

// category routes
routes.post('/category', checkAuth, categoryController.createCategory());
routes.get('/category', checkAuth, categoryController.allCategories());
routes.put('/category', checkAuth, categoryController.updateCategory());
routes.delete('/category', checkAuth, categoryController.deleteCategory());
routes.get('/category', checkAuth, categoryController.getCategory());

// admin
routes.post('/manager', managerAuthController.manageAuth());
routes.get('/manager', managerAuthController.getManager());

// customer router
routes.post('/customer', customerController.customer());

routes.post('/customer/auth', customerController.getCustomer());
routes.post('/customer/auth/:id', customerController.uploadGesture());

// report
routes.get('/report/sales', reportController.reportSales());
routes.get('/report/category', reportController.reportCategories())
routes.get('/report/customers', reportController.reportCustomers());

module.exports = routes;