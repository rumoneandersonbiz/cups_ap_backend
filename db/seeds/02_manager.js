
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('manager').del()
    .then(function () {
      // Inserts seed entries
      return knex('manager').insert(
        { user_id: 1,  manager_email: 'shannon@gmail.com', is_current: true });
    });
};
