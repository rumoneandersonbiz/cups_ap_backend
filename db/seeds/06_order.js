
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('customer_order').del()
    .then(function () {
      // Inserts seed entries
      return knex('customer_order').insert([
        {customer_id: 1, item_id: 1, order_time: new Date(), is_complete: false, is_cancelled: false},
        {customer_id: 1, item_id: 1, order_time: new Date(), is_complete: false, is_cancelled: true},
      ]);
    });
};
