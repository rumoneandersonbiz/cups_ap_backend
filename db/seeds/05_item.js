
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('item').del()
    .then(function () {
      // Inserts seed entries
      return knex('item').insert({
          manager_id: 1,
          category_id: 1,
          item_name: 'Mocha Vanilla',
          item_cost: 12,
          quantity: 11,
          sign_photo: '/items/MAR-30-2020-04:21:49-CUPSLOGONOBACKGROUND.PNG',
          item_photo: '/items/MAR-30-2020-04:21:49-MYPROFILE.JPG',
          item_audio: '/items/sound.mp3',
          sign_photo_hash: '0bb3b6d2cf42993443ba59ab4b366684',
          item_photo_hash: '4882b8a8b93aec7fdf9939651e169c51',
          item_audio_hash: '646c184ba42e3bd4107291dff8dd1202'
        });
    });
};
