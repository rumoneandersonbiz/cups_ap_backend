
exports.seed = function(knex) {
  return knex('application_user').del()
  .then(function () {
    return knex('application_user').insert([
      {
        // user_id: 1,
        firstName: 'Shannon',
        lastName: 'Henry'
      },
      {
        // user_id: 2,
        firstName: 'Rumone',
        lastName: 'Anderson'
      },
      {
        // user_id: 3,
        firstName: 'Rennae',
        lastName: 'Sammuels'
      }
    ])
  })
};
