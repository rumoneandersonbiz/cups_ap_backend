
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('customer').del()
    .then(function () {
      // Inserts seed entries
      return knex('customer').insert([
        {
          user_id: 2,
          cups_coin: 500,
          disability: 'DEAF',
          isAuth: true,
          auth_gesture_hash: '0bb3b6d2cf42993443ba59ab4b366684'
        },
      ]);
    });
};
