
exports.up = function(knex) {
    return knex.schema.createTable('customer_order', table => {
        table.increments('order_id');
        table.integer('customer_id').notNullable();
        table.integer('item_id').notNullable();
        table.timestamp('order_time').notNullable();
        table.boolean('is_complete').defaultTo(false);
        table.boolean('is_cancelled').defaultTo(false);
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('customer_order')
};
