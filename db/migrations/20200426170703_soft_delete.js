
exports.up = knex => knex.schema.alterTable('item', tb => {
    tb.boolean('is_deleted').defaultTo(false);
})

exports.down = knex => knex.schema.alterTable('item', tb => {
    tb.dropColumn('is_deleted')
})
