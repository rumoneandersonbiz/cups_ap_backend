
exports.up = function(knex) {
    return knex.schema.createTable('manager', table => {
        table.increments('manager_id');
        table.integer('user_id').references('user_id').inTable('application_user').notNullable().unique();
        table.string('manager_email').unique();
        table.boolean('is_current');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('manager');
};
