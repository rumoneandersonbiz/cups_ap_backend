
exports.up = function(knex) {
    return knex.schema.alterTable('customer', table => {
        table.boolean('isAuth').notNullable();
    })
};

exports.down = function(knex) {
    return knex.schema.alterTable('customer', table => {
        table.dropColumn('isAuth');
    })
};
