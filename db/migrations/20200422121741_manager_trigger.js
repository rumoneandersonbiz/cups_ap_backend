const ON_CREATE_NEW_MANAGER = `
    CREATE OR REPLACE FUNCTION on_new_manager()
    RETURNS trigger AS
    $BODY$
    BEGIN
        UPDATE manager
        SET is_current=false
        WHERE is_current=true;
        RETURN NEW;
    END;
    $BODY$
    language 'plpgsql';
`

const ON_REMOVE_NEW_MANAGER_FUNCTION = `DROP FUNCTION on_new_manager`

exports.up = function(knex) {
    return knex.raw(ON_CREATE_NEW_MANAGER)
};

exports.down = function(knex) {
    return knex.raw(ON_REMOVE_NEW_MANAGER_FUNCTION)
};
