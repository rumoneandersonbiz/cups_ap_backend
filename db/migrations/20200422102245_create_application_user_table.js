
exports.up = function(knex) {
  return knex.schema.createTable('application_user', table => {
      table.increments('user_id');
      table.string('firstName').notNullable();
      table.string('lastName').notNullable();
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('application_user')
};
