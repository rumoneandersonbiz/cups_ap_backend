const CALL_TRIGGER_NEW_MANAGER = `
    CREATE TRIGGER manager_change
    BEFORE INSERT
    ON manager
    EXECUTE PROCEDURE on_new_manager();
`
const DROP_TRIGGER_NEW_MANAGER = `
    DROP TRIGGER manager_change ON manager
`

exports.up = knex => knex.raw(CALL_TRIGGER_NEW_MANAGER)

exports.down = knex => knex.raw(DROP_TRIGGER_NEW_MANAGER)