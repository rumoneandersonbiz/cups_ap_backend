
exports.up = function(knex) {
    return knex.schema.createTable('customer', table => {
        table.increments('customer_id');
        table.integer('user_id').references('user_id').inTable('application_user').notNullable().unique();
        table.decimal('cups_coin').notNullable();
        table.string('disability').notNullable();
        table.string('auth_gesture_hash');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('customer')
};
