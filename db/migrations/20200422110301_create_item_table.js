
exports.up = function(knex) {
    return knex.schema.createTable('item', table => {
        table.increments('item_id');
        table.integer('manager_id').references('manager_id').inTable('manager').notNullable();
        table.integer('category_id').references('category_id').inTable('category').notNullable();
        table.string('item_name').notNullable().unique();
        table.decimal('item_cost').notNullable();
        table.integer('quantity').notNullable();
        table.string('sign_photo').notNullable();
        table.string('item_photo').notNullable();
        table.string('item_audio').notNullable();
        table.string('sign_photo_hash').notNullable().unique();
        table.string('item_photo_hash').notNullable().unique();
        table.string('item_audio_hash').notNullable().unique();
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('item')
};
