
exports.up = knex => knex.schema.alterTable('customer', tb => {
    tb.string('auth_gesture_hash').unique().alter();
})

exports.down = knex => knex.schema.alterTable('customer', tb => {
    tb.string('auth_gesture_hash').alter();
});
