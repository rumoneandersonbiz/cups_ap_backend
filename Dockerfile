FROM node:12.16.0-alpine3.11

# sets the current working directory
WORKDIR /usr/src/cup_api
# copy the root of the project to the container
COPY ./ ./

# install the dependencies
RUN yarn

