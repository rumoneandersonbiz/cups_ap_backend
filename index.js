const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const fileUpload = require('express-fileupload')

const PORT = process.env.PORT || 3000;

// custom check authentication middleware
const checkAuth = require('./firebase')

const routes = require('./routes')

const app = express();

// middlewares
app.use(express.static('uploads'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload({
    createParentPath: true,
    useTempFiles: true,
    tempFileDir: '/tmp/',
    debug: true
}))
app.use(morgan('dev'));
app.use(cors());

app.use('/api/', routes);

// gets the home route
app.get('/', checkAuth, (req, res) => {
    res.json({
        message: "Foo Bar: Hey shannon this is our project"
    })
});

 
app.listen(PORT, () => {
    console.log(`[==> LISTENING on port: ${PORT}]`)
});
