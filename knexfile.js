// Update with your config settings.
const localConnectionString = 'postgresql://postgres:password@localhost:5432/cups_ap';
const productionConnectionString = 'postgres://abozqhel:QxBlO3ffxPM3jm2Hg5JSh3osj9NC393o@drona.db.elephantsql.com:5432/abozqhel';

module.exports = {
  development: {
    client: 'pg',
    connection: localConnectionString, 
    migrations: {
      directory: './db/migrations'
    },
    seeds: {
      directory: './db/seeds'
    },
    useNullAsDefault: true
  },

  staging: {
    client: 'pg',
    connection: process.env.POSTGRES_URI, 
    migrations: {
      directory: './db/migrations'
    },
    seeds: {
      directory: './db/seeds'
    },
    useNullAsDefault: true
  },

  production: {
    client: 'pg',
    connection: productionConnectionString, 
    migrations: {
      directory: './db/migrations'
    },
    seeds: {
      directory: './db/seeds'
    },
    useNullAsDefault: true
  }
};
